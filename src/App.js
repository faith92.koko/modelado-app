import React, { Component } from 'react';
import axios from 'axios';
import ReactWordCloud from 'react-wordcloud';

import './App.css';

const WORD_COUNT_KEY = 'frecuencia';
const WORD_KEY = 'palabra';

class App extends Component {
  constructor () {
    super();

    this.state = {
      palabras: [],
      estaRecibiendo: true,
      cantidad: 0,
      batch: 0
    };
  }

  componentDidMount () {
    setInterval(() => {
      axios.get('http://127.0.0.1:3333/palabras')
      .then((response) => {
        const { data: { data, code, batch } } = response;

        if (code === 200) {
          if (batch !== this.state.batch) {
            this.setState({
              palabras: data,
              estaRecibiendo: false,
              batch
            });
          }
        }
      });
    }, 700);
  }

  render() {
    const { palabras, estaRecibiendo } = this.state;

    return (
      <div className="App">
        <div style={{ backgroundColor: '#1E88E5', padding: 12, display: 'flex', flexDirection: 'column' }}>
          <span style={{ color: '#FFFFFF', fontSize: 20 }}>
            Ingeniería en Sistemas Computacionales
          </span>
          <span style={{ color: '#EEEEEE', fontSize: 16 }}>
            Modelado de aplicaciones
          </span>
        </div>
        <div style={{ padding: 12 }}>
          {
            estaRecibiendo && (
              <span style={{ fontWeight: 'bold', color: '#BDBDBD', fontSize: 30 }}>Esperando que llegen palabras al servidor =)...</span>
            )
          }
        </div>
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <ReactWordCloud
            minAngle={-60}
            maxAngle={60}
            orientations={5}
            words={palabras}
            height={800}
            width={800}
            wordCountKey={WORD_COUNT_KEY}
            wordKey={WORD_KEY}
            maxWords={300}
          />
        </div>
      </div>
    );
  }
}

export default App;
